﻿using ODS.ServiceContract.DataContract;
using ODS.ServiceContract.ViewModels.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ODS.ServiceContract
{
    /// <summary>
    /// The IEntityService
    /// </summary>
    [ServiceContract]
    public interface IEntityService
    {
        #region Public Methods SuperAdmin Part

        /// <summary>
        /// Get entity by id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(ErrorDetails))]
        EntityModelView Entity(long entityId);

        /// <summary>
        /// Set for the entity value block/not block
        /// </summary>
        /// <param name="entityId">The entityId</param>
        /// <param name="isBlocked">The isBlocked</param>
        [OperationContract]
        [FaultContract(typeof(ErrorDetails))]
        void EntityBlocked(long entityId, bool isBlocked);

        /// <summary>
        /// Making the company is official
        /// </summary>
        /// <param name="entityId">The entityId</param>
        [OperationContract]
        [FaultContract(typeof(ErrorDetails))]
        void EntityOfficial(long entityId);
        
        /// <summary>
        /// Send warn for entity
        /// </summary>
        /// <param name="entityId"></param>
        [OperationContract]
        [FaultContract(typeof(ErrorDetails))]
        void EntityWarn(long entityId);

        /// <summary>
        /// List of thr all entities
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(ErrorDetails))]
        List<EntityAdminView> Entities();

        /// <summary>
        /// Get non confirm entities
        /// </summary>
        /// <param name="pagination">The pagination</param>
        /// <returns></returns>
        [OperationContract]
        [FaultContract(typeof(ErrorDetails))]
        List<EntityNonConfirmAdminView> EntitiesNonConfirm();

        /// <summary>
        /// Approve entity
        /// </summary>
        /// <param name="entityNonAprovedId"></param>
        /// <returns></returns>
        //EntityAprovedView EntityAproved(long entityNonAprovedId);

        #endregion

        #region Public Methods Web Part

        [OperationContract]
        [FaultContract(typeof(ErrorDetails))]
        void Create(EntityCreate request);
        
        #endregion
    }
}
