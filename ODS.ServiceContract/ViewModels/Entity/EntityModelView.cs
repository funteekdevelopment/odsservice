﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODS.ServiceContract.ViewModels.Entity
{
    /// <summary>
    /// The EntityModelView
    /// </summary>
    public class EntityModelView
    {
        #region Property

        /// <summary>
        /// The Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The CreateUserID
        /// </summary>
        public long CreateUserID { get; set; }

        /// <summary>
        /// The Image
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// The IsDeleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// The WebSite
        /// </summary>
        public string WebSite { get; set; }

        /// <summary>
        /// The Phone
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// The About
        /// </summary>
        public string About { get; set; }

        /// <summary>
        /// The IsApproved
        /// </summary>
        public bool IsApproved { get; set; }

        /// <summary>
        /// The Type
        /// </summary>
        public Enums.Type TypeEntity { get; set; }

        /// <summary>
        /// The IsOfficial
        /// </summary>
        public bool IsOfficial { get; set; }

        /// <summary>
        /// The IsBlocked
        /// </summary>
        public bool IsBlocked { get; set; }

        /// <summary>
        /// The Fb
        /// </summary>
        public string Fb { get; set; }

        /// <summary>
        /// The IsWarn
        /// </summary>
        public bool IsWarn { get; set; }

        /// <summary>
        /// The Email
        /// </summary>
        public string Email { get; set; }

        #endregion
    }
}
