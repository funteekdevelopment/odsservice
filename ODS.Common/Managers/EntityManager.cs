﻿using ODS.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODS.DataAccess.Models;
using ODS.DataAccess.Context;
using ODS.Common.Logging;
using System.Data.Entity.Validation;
using ODS.Common.Infrastructure;
using ODS.Common.Helpers;

namespace ODS.Common.Managers
{
    /// <summary>
    /// The EntityManager
    /// </summary>
    public class EntityManager : IEntityManager
    {
        /// <summary>
        /// The xmlHelper
        /// </summary>
        private IXmlHelper _xmlHelper;

        /// <summary>
        /// The XmlHelper
        /// </summary>
        private IXmlHelper xmlHelper
        {
            get
            {
                if (this._xmlHelper == null)
                {
                    this._xmlHelper = new ODS.Common.Helpers.XmlHelper();

                    return this._xmlHelper;
                }

                return this._xmlHelper;
            }
        }

        /// <summary>
        /// The logger
        /// </summary>
        private ILogger _logger;

        /// <summary>
        /// The Logger
        /// </summary>
        private ILogger logger
        {
            get
            {
                if (this._logger == null)
                {
                    this._logger = new ODS.Common.Logging.Logger();

                    return this._logger;
                }

                return this._logger;
            }
        }

        /// <summary>
        /// Creating new entity
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="db"></param>
        public void CreateEntity(Entity entity, FunteekDbContext db)
        {
            db.AddEntity(entity);

            db.SaveChanges();
        }

        /// <summary>
        /// Creating new manager
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="db"></param>
        public void CreateManager(Manager manager, FunteekDbContext db)
        {
            db.AddEntity(manager);

            db.SaveChanges();
        }
    }
}
