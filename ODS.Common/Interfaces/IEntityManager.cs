﻿using ODS.DataAccess.Context;
using ODS.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODS.Common.Interfaces
{
    /// <summary>
    /// The IEntityManager
    /// </summary>
    public interface IEntityManager
    {
        /// <summary>
        /// Creating new entity
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="db"></param>
        void CreateEntity(Entity entity, FunteekDbContext db);

        /// <summary>
        /// Creating new manager
        /// </summary>
        /// <param name="manager"></param>
        /// <param name="db"></param>
        void CreateManager(Manager manager, FunteekDbContext db);
    }
}
