﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODS.Common.Infrastructure
{
    public class Constants
    {
        /// <summary>
        /// Constant for InternalServerError.
        /// </summary>
        public const string InternalServerError = "InternalServerError";

        public const string ClientError = "ClientError";

        /// <summary>
        /// Constant for ValidationModelError.
        /// </summary>
        public const string ValidationModelError = "ValidationModelError";
    }
}
