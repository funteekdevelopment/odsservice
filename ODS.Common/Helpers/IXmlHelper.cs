﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODS.Common.Helpers
{
    /// <summary>
    /// The IXmlHelper
    /// </summary>
    public interface IXmlHelper
    {
        /// <summary>
        /// Serialixing object to xml
        /// </summary>
        /// <param name="dataObject"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        string SerializeObjectToXml(object dataObject, Encoding encoding = null);
    }
}
