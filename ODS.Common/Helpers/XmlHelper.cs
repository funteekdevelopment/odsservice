﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ODS.Common.Helpers
{
    /// <summary>
    /// The XmlHelper
    /// </summary>
    public class XmlHelper : IXmlHelper
    {
        /// <summary>
        /// Serializing object to xml 
        /// </summary>
        /// <param name="dataObject"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public string SerializeObjectToXml(object dataObject, Encoding encoding = null)
        {
            var resultStr = string.Empty;
            
            if (encoding == null)
            {
                encoding = Encoding.UTF8;
            }
            
            if (dataObject != null)
            {
                using (var memoryStream = new MemoryStream())
                {
                    var serializer = new DataContractSerializer(dataObject.GetType());
                    serializer.WriteObject(memoryStream, dataObject);
                    resultStr = encoding.GetString(memoryStream.ToArray());
                }
            }

            return resultStr;
        }
    }
}
