﻿using ODS.DataAccess.Models;
using ODS.ServiceContract.ViewModels.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODS.Common.Converters
{
    /// <summary>
    /// The IEntityConverter
    /// </summary>
    public interface IEntityConverter
    {
        /// <summary>
        /// The ModelToView
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        EntityModelView ModelToView(Entity entity);
    }
}
