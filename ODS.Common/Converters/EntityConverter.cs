﻿using ODS.DataAccess.Models;
using ODS.ServiceContract.ViewModels.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODS.Common.Converters
{
    /// <summary>
    /// The EntityConverter
    /// </summary>
    public class EntityConverter : IEntityConverter
    {
        /// <summary>
        /// The ModelToView
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public EntityModelView ModelToView(Entity entity)
        {
            return new EntityModelView
            {
                About = entity.About,
                CreateUserID = entity.CreateUserID,
                Email = entity.Email,
                Fb = entity.Fb,
                Image = entity.Image,
                IsApproved = entity.IsApproved,
                IsBlocked = entity.IsBlocked,
                IsDeleted = entity.IsDeleted,
                IsOfficial = entity.IsOfficial,
                IsWarn = entity.IsWarn,
                Phone = entity.Phone,
                Title = entity.Title,
                TypeEntity = entity.TypeEntity,
            };
        }
    }
}
