﻿using log4net;
using ODS.Common.Converters;
using ODS.Common.Helpers;
using ODS.Common.Infrastructure;
using ODS.Common.Interfaces;
using ODS.Common.Logging;
using ODS.DataAccess.Context;
using ODS.DataAccess.Models;
using ODS.ServiceContract;
using ODS.ServiceContract.DataContract;
using ODS.ServiceContract.ViewModels.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ODS.WCFService
{
    /// <summary>
    /// The EntityService
    /// </summary>
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple, InstanceContextMode = InstanceContextMode.Single)]
    public class EntityService : IEntityService
    {
        #region Fields

        /// <summary>
        /// The xmlHelper
        /// </summary>
        private IXmlHelper _xmlHelper;

        /// <summary>
        /// The entityManager
        /// </summary>
        private IEntityManager _entityManager;

        /// <summary>
        /// The logger
        /// </summary>
        private ILogger _logger;

        /// <summary>
        /// The converter
        /// </summary>
        private IEntityConverter converter;

        #endregion

        #region Property

        /// <summary>
        /// The XmlHelper
        /// </summary>
        private IXmlHelper xmlHelper
        {
            get
            {
                if (this._xmlHelper == null) this._xmlHelper = new XmlHelper(); 

                return this._xmlHelper;
            }
        }
        
        /// <summary>
        /// The EntityManager
        /// </summary>
        private IEntityManager entityManager
        {
            get
            {
                if (this._entityManager != null) return this._entityManager;

                this._entityManager = new ODS.Common.Managers.EntityManager();

                return this._entityManager;
            }
        }

        /// <summary>
        /// The Logger
        /// </summary>
        private ILogger logger
        {
            get
            {
                if (this._logger != null) return this._logger;

                this._logger = new ODS.Common.Logging.Logger();

                return this._logger;
            }
        }

        /// <summary>
        /// The Converter
        /// </summary>
        private IEntityConverter Converter
        {
            get
            {
                if (this.converter != null) return this.converter;

                this.converter = new EntityConverter();

                return this.converter;
            }
        }

        #endregion

        #region Constructors

        /// <summary>
        /// Initializing EntityService
        /// </summary>
        public EntityService() { }

        public EntityService(IXmlHelper xmlHelper, ILogger logger, IEntityManager entityManager)
        {
            this._xmlHelper = xmlHelper;
            this._logger = logger;
            this._entityManager = entityManager;
        }

        #endregion

        #region Public Methods SuperAdmin Part

        /// <summary>
        /// Get entity by id
        /// </summary>
        /// <param name="entityId"></param>
        /// <returns></returns>
        public EntityModelView Entity(long entityId)
        {
            try
            {
                using (var db = new FunteekDbContext())
                {
                    var entity = db.Entities.FirstOrDefault(item => item.Id == entityId && item.IsDeleted == false);

                    return this.Converter.ModelToView(entity);
                }
            }
            catch (Exception ex)
            {
                logger.Error(Constants.InternalServerError, ex);

                var fault = new ErrorDetails(ex.Message, ex.StackTrace);

                throw new FaultException<ErrorDetails>(fault, new FaultReason(Constants.InternalServerError));
            }
        }

        /// <summary>
        /// set for the entity value block/not block
        /// </summary>
        /// <param name="entityId">The entityId</param>
        /// <param name="isBlocked">The isBlocked</param>
        public void EntityBlocked(long entityId, bool isBlocked)
        {
            try
            {
                using (var db = new FunteekDbContext())
                {
                    var entity = db.Entities.FirstOrDefault(item => item.Id == entityId && item.IsDeleted == false);

                    entity.IsBlocked = isBlocked;

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error(Constants.InternalServerError, ex);

                var fault = new ErrorDetails(ex.Message, ex.StackTrace);

                throw new FaultException<ErrorDetails>(fault, new FaultReason(Constants.InternalServerError));
            }
        }

        /// <summary>
        /// making the company is official
        /// </summary>
        /// <param name="entityId">The entityId</param>
        public void EntityOfficial(long entityId)
        {
            try
            {
                using (var db = new FunteekDbContext())
                {
                    var entity = db.Entities.FirstOrDefault(item => item.Id == entityId && item.IsDeleted == false);

                    entity.IsOfficial = true;

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error(Constants.InternalServerError, ex);

                var fault = new ErrorDetails(ex.Message, ex.StackTrace);

                throw new FaultException<ErrorDetails>(fault, new FaultReason(Constants.InternalServerError));
            }
        }
        
        /// <summary>
        /// Send warn for entity
        /// </summary>
        /// <param name="entityId"></param>
        public void EntityWarn(long entityId)
        {
            try
            {
                using (var db = new FunteekDbContext())
                {
                    var entity = db.Entities.FirstOrDefault(item => item.Id == entityId && item.IsDeleted == false);
                    
                    entity.IsWarn = true;

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                logger.Error(Constants.InternalServerError, ex);

                var fault = new ErrorDetails(ex.Message, ex.StackTrace);

                throw new FaultException<ErrorDetails>(fault, new FaultReason(Constants.InternalServerError));
            }
        }

        /// <summary>
        /// List of thr all entities
        /// </summary>
        /// <returns></returns>
        public List<EntityAdminView> Entities()
        {
            try
            {
                var result = new List<EntityAdminView>();

                using (var db = new FunteekDbContext())
                {
                    var entities = db.Entities.Where(item => item.IsDeleted == false && item.IsBlocked == false).ToList();

                    result.AddRange(entities.Select(item => new EntityAdminView
                    {
                        //count_complaint = this.complaintRepository.CountEntityComplaint(item.ID),
                        id = item.Id,
                        image = item.Image,
                        is_official = item.IsOfficial,
                        title = item.Title,
                        is_warn = item.IsWarn
                    }));
                }

                return result;
            }
            catch (Exception ex)
            {
                logger.Error(Constants.InternalServerError, ex);

                var fault = new ErrorDetails(ex.Message, ex.StackTrace);

                throw new FaultException<ErrorDetails>(fault, new FaultReason(Constants.InternalServerError));
            }
        }

        /// <summary>
        /// Get all non confirm entities
        /// </summary>
        /// <returns></returns>
        public List<EntityNonConfirmAdminView> EntitiesNonConfirm()
        {
            try
            {
                logger.Debug("ODS.WCFService.EntityService : EntitiesNonConfirm call");

                var res = new List<EntityNonConfirmAdminView>();

                using (var db = new FunteekDbContext())
                {
                    var list = db.EntitiesNonConfirm.Where(item => item.IsDeleted == false).OrderByDescending(e => e.CreateDate).ToList();

                    res.AddRange(list.Select(item => new EntityNonConfirmAdminView
                    {
                        email = item.Email,
                        id = item.Id,
                        name_company = item.NameCompany,
                        phone = item.Phone,
                        web_site = item.WebSite
                    }));
                }

                logger.Debug("ODS.WCFService.EntityService : EntitiesNonConfirm finished successfully");
                
                return res;
            }
            catch(Exception ex)
            {
                logger.Error(Constants.InternalServerError, ex);

                var fault = new ErrorDetails(ex.Message, ex.StackTrace);

                throw new FaultException<ErrorDetails>(fault, new FaultReason(Constants.InternalServerError));
            }
        }

        #endregion

        #region Public Methods Web Part

        /// <summary>
        /// Creating entity
        /// </summary>
        /// <param name="entity"></param>
        public void Create(EntityCreate request)
        {
            try
            {
                using (var db = new FunteekDbContext())
                {
                    logger.Debug(string.Format("ODS.WCFService.EntityService : Create call, request = {0}", this.xmlHelper.SerializeObjectToXml(request)));

                    var entity = new Entity
                    {
                        Image = request.Image,
                        Title = request.Title,
                        WebSite = request.WebSite,
                        Phone = request.Phone,
                        About = request.About,
                        Fb = request.Fb,
                        Email = request.Email,
                        TypeEntity = (ServiceContract.Enums.Type)Enum.Parse(typeof(ServiceContract.Enums.Type), request.Type),
                        CreateUserID = request.CreateUserID
                    };

                    this.entityManager.CreateEntity(entity, db);

                    var manager = new Manager(request.CreateUserID, entity.Id, (long)Manager.Roles.Admin);

                    this.entityManager.CreateManager(manager, db);

                    logger.Debug(string.Format("ODS.WCFService.EntityService : Create finished successfully"));
                }
            }
            catch(Exception ex)
            {
                logger.Error(Constants.InternalServerError, ex);

                var fault = new ErrorDetails(ex.Message, ex.StackTrace);

                throw new FaultException<ErrorDetails>(fault, new FaultReason(Constants.InternalServerError));
            }
        }

        #endregion
    }
}
