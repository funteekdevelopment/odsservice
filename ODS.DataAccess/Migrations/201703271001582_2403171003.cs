namespace ODS.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2403171003 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.tEntity", "Title", c => c.String());
            AlterColumn("dbo.tEntity", "Image", c => c.String());
            AlterColumn("dbo.tEntity", "WebSite", c => c.String());
            AlterColumn("dbo.tEntity", "Phone", c => c.String());
            AlterColumn("dbo.tEntity", "Fb", c => c.String());
            AlterColumn("dbo.tEntity", "Email", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.tEntity", "Email", c => c.String(nullable: false));
            AlterColumn("dbo.tEntity", "Fb", c => c.String(nullable: false));
            AlterColumn("dbo.tEntity", "Phone", c => c.String(nullable: false));
            AlterColumn("dbo.tEntity", "WebSite", c => c.String(nullable: false));
            AlterColumn("dbo.tEntity", "Image", c => c.String(nullable: false));
            AlterColumn("dbo.tEntity", "Title", c => c.String(nullable: false));
        }
    }
}
