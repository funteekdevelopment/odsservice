namespace ODS.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2403171004 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.tEntity", "About", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.tEntity", "About", c => c.String(nullable: false));
        }
    }
}
