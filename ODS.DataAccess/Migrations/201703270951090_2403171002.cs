namespace ODS.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2403171002 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.tEntity", "WebSite", c => c.String(nullable: false));
            AlterColumn("dbo.tEntity", "Phone", c => c.String(nullable: false));
            AlterColumn("dbo.tEntity", "About", c => c.String(nullable: false));
            AlterColumn("dbo.tEntity", "Fb", c => c.String(nullable: false));
            AlterColumn("dbo.tEntity", "Email", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.tEntity", "Email", c => c.String());
            AlterColumn("dbo.tEntity", "Fb", c => c.String());
            AlterColumn("dbo.tEntity", "About", c => c.String());
            AlterColumn("dbo.tEntity", "Phone", c => c.String());
            AlterColumn("dbo.tEntity", "WebSite", c => c.String());
        }
    }
}
