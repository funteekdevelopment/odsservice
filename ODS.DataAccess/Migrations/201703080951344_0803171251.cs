namespace ODS.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0803171251 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.tEntity", "CurrencyId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.tEntity", "CurrencyId", c => c.Long(nullable: false));
        }
    }
}
