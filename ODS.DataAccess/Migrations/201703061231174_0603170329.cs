namespace ODS.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0603170329 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.tEntity",
                c => new
                    {
                        EntityId = c.Long(nullable: false, identity: true),
                        Title = c.String(),
                        CreateUserID = c.Long(nullable: false),
                        Image = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        WebSite = c.String(),
                        Phone = c.String(),
                        About = c.String(),
                        IsApproved = c.Boolean(nullable: false),
                        Type = c.String(),
                        IsOfficial = c.Boolean(nullable: false),
                        IsBlocked = c.Boolean(nullable: false),
                        Fb = c.String(),
                        IsWarn = c.Boolean(nullable: false),
                        TypeId = c.Long(nullable: false),
                        CurrencyId = c.Long(nullable: false),
                        Email = c.String(),
                        CreateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.EntityId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.tEntity");
        }
    }
}
