namespace ODS.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0803171224 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.tEntity", "TypeEntity", c => c.Int(nullable: false));
            DropColumn("dbo.tEntity", "Type");
        }
        
        public override void Down()
        {
            AddColumn("dbo.tEntity", "Type", c => c.String());
            DropColumn("dbo.tEntity", "TypeEntity");
        }
    }
}
