namespace ODS.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0803171257 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.tEntity", "TypeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.tEntity", "TypeId", c => c.Long(nullable: false));
        }
    }
}
