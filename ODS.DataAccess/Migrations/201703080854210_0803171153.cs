namespace ODS.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0803171153 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.EntityNonConfirms", newName: "tEntitiesNonConfirm");
            RenameColumn(table: "dbo.tEntitiesNonConfirm", name: "Id", newName: "EntitiesNonConfirmId");
            CreateTable(
                "dbo.tEntityManagers",
                c => new
                    {
                        EntityManagersId = c.Long(nullable: false, identity: true),
                        EntityId = c.Long(nullable: false),
                        UserId = c.Long(nullable: false),
                        RoleId = c.Long(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.EntityManagersId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.tEntityManagers");
            RenameColumn(table: "dbo.tEntitiesNonConfirm", name: "EntitiesNonConfirmId", newName: "Id");
            RenameTable(name: "dbo.tEntitiesNonConfirm", newName: "EntityNonConfirms");
        }
    }
}
