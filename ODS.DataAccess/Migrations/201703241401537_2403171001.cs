namespace ODS.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _2403171001 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.tEntity", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.tEntity", "Image", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.tEntity", "Image", c => c.String());
            AlterColumn("dbo.tEntity", "Title", c => c.String());
        }
    }
}
