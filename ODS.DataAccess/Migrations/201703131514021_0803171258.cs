namespace ODS.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0803171258 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.tEntityManagers", newName: "tManager");
            RenameColumn(table: "dbo.tManager", name: "EntityManagersId", newName: "ManagerId");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.tManager", name: "ManagerId", newName: "EntityManagersId");
            RenameTable(name: "dbo.tManager", newName: "tEntityManagers");
        }
    }
}
