namespace ODS.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _0703170153 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.EntityNonConfirms",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        NameCompany = c.String(),
                        WebSite = c.String(),
                        Email = c.String(),
                        Phone = c.String(),
                        IsDeleted = c.Boolean(nullable: false),
                        CreateDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.EntityNonConfirms");
        }
    }
}
