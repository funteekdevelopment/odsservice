﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODS.DataAccess.Models
{
    public class EntityNonConfirm : EntryBase
    {
        public string NameCompany { get; set; }

        public string WebSite { get; set; }

        public string Email { get; set; }

        public string Phone { get; set; }

        public bool IsDeleted { get; set; }
    }
}
