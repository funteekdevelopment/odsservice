﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ODS.ServiceContract;

namespace ODS.DataAccess.Models
{
    /// <summary>
    /// The Entity
    /// </summary>
    public class Entity : EntryBase
    {
        #region Property

        /// <summary>
        /// The Title
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// The CreateUserID
        /// </summary>
        public long CreateUserID { get; set; }

        /// <summary>
        /// The Image
        /// </summary>
        public string Image { get; set; }

        /// <summary>
        /// The IsDeleted
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// The WebSite
        /// </summary>
        public string WebSite { get; set; }

        /// <summary>
        /// The Phone
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// The About
        /// </summary>
        public string About { get; set; }

        /// <summary>
        /// The IsApproved
        /// </summary>
        public bool IsApproved { get; set; }

        /// <summary>
        /// The Type
        /// </summary>
        public ServiceContract.Enums.Type TypeEntity { get; set; }

        /// <summary>
        /// The IsOfficial
        /// </summary>
        public bool IsOfficial { get; set; }

        /// <summary>
        /// The IsBlocked
        /// </summary>
        public bool IsBlocked { get; set; }

        /// <summary>
        /// The Fb
        /// </summary>
        public string Fb { get; set; }

        /// <summary>
        /// The IsWarn
        /// </summary>
        public bool IsWarn { get; set; }

        /// <summary>
        /// The Email
        /// </summary>
        public string Email { get; set; }

        #endregion

        //#region Constructors

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="image"></param>
        ///// <param name="title"></param>
        ///// <param name="webSite"></param>
        ///// <param name="phone"></param>
        ///// <param name="about"></param>
        ///// <param name="fb"></param>
        ///// <param name="email"></param>
        ///// <param name="type"></param>
        ///// <param name="createUserID"></param>
        //public Entity(string image, string title, string webSite, string phone, string about, string fb, string email, ServiceContract.Enums.Type type, long createUserID)
        //{
        //    this.Image = image;
        //    this.Title = Title;
        //    this.WebSite = webSite;
        //    this.Phone = phone;
        //    this.About = about;
        //    this.WebSite = fb;
        //    this.Image = email;
        //    this.TypeEntity = type;
        //    this.CreateUserID = createUserID;
        //}

        //#endregion
    }
}
