﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODS.DataAccess.Models
{
    /// <summary>
    /// The Manager
    /// </summary>
    public class Manager : EntryBase
    {
        #region Property

        /// <summary>
        /// The EntityId
        /// </summary>
        public long EntityId { get; set; }

        /// <summary>
        /// The UserId
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// The RoleId
        /// </summary>
        public long RoleId { get; set; }

        #endregion

        #region Constructors

        public Manager(long entityId, long userId, long roleId)
        {
            this.EntityId = entityId;
            this.UserId = userId;
            this.RoleId = roleId;
        }

        #endregion

        #region Enums

        /// <summary>
        /// Roles for managers
        /// </summary>
        public enum Roles : long
        {
            Admin = 1,
            Manager = 2,
            Operator = 3
        }

        #endregion
    }
}
