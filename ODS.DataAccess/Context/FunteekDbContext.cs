﻿using ODS.DataAccess.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ODS.DataAccess.Context
{
    /// <summary>
    /// The FunteekDbContext
    /// </summary>
    public class FunteekDbContext : DbContext
    {
        # region Property

        /// <summary>
        /// 
        /// </summary>
        public DbSet<Entity> Entities { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<EntityNonConfirm> EntitiesNonConfirm { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<Manager> Manager { get; set; }
        

        #endregion

        #region Constructors

        /// <summary>
        /// Initializing FunteekDbContext
        /// </summary>
        public FunteekDbContext() : base("FunteekTestDB")
        {

        }

        #endregion

        #region Protected Methods 

        /// <summary>
        /// Creating tables in db
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            InitializeEntity<Entity>(modelBuilder, "tEntity", "EntityId");
            InitializeEntity<EntityNonConfirm>(modelBuilder, "tEntitiesNonConfirm", "EntitiesNonConfirmId");
            InitializeEntity<Manager>(modelBuilder, "tManager", "ManagerId");
        }

        #endregion

        #region Private Methods 

        /// <summary>
        /// Initializing tables
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="modelBuilder"></param>
        /// <param name="tableName"></param>
        /// <param name="dataIDFieldName"></param>
        private void InitializeEntity<T>(DbModelBuilder modelBuilder, string tableName, string dataIDFieldName) where T : EntryBase
        {
            modelBuilder.Entity<T>().ToTable(tableName);
            modelBuilder.Entity<T>().HasKey(d => d.Id);
            modelBuilder.Entity<T>().Property(d => d.Id).HasColumnName(dataIDFieldName);
        }

        /// <summary>
        /// Add new entity in table
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        public void AddEntity<T>(T entity) where T : EntryBase
        {
            entity.CreateDate = DateTime.UtcNow;
            Set<T>().Add(entity);
        }

        #endregion
    }
}
